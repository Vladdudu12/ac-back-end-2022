///VARIABILE

//aratam ca la const trebuie initializat inainte de a putea fi folosit

let x
x = 5
console.log(x);
const y = 3 //aici da eroare daca nu initializam

//explicam pe scurt de ce nu se foloseste var
var z  //explicam ca standard-ul este cu let

x = "da" //string
console.log(x);
console.log(typeof(x));
x = true //bool
console.log(x);
console.log(typeof(x));
x = 2.3 //float
console.log(x);
console.log(typeof(x));
x = 5 //int
console.log(x);
console.log(typeof(x));

y = 8
console.log(y);


x = x + 1
x += 1


let p = 5

console.log(p++)
console.log(p)
//OPERATORI

z = "5"

let adevarat = true;
let fals = false;

console.log(x > y)
console.log(x < y)

console.log(x >= y)
console.log(x <= y)

console.log(x == y)
console.log(x === z)

console.log(x != y)

console.log(adevarat || fals)
console.log(adevarat && fals)

console.log(x == 5 && y == 3)
console.log(x == 5 && y == 5)

console.log(x == 5 || y == 5)
console.log(x == 3 || y == 3)


console.log(adevarat ? "da" : "nu")
console.log(fals ? "da" : "nu")


//STRUCTURA DECIZIONALA
//TOOD: ADAUGAT ACOLADE
if(x == 5)
    console.log("x este egal cu 5")
if(x == 3)
    console.log("x este egal cu 3")

if(adevarat == true)
    console.log("Am intrat in if")
if(adevarat != true)
    console.log("Si aici am intrat in if")

if(adevarat)
    console.log("Chiar am intrat in if")

if(!adevarat)
    console.log("Oare am intrat in if?")

if(x == 5)
    if(y == 3)
        console.log("x este egal cu 5 si y este egal cu 3")


if(x == 5)
    console.log("x este egal cu 5")
else
    console.log("x nu este egal cu 5")

if(x == 5){
    console.log("x este egal cu 5")
}
else if(y == 3){
    console.log("x nu este egal cu 5, dar y este egal cu 3")
}
else{
    console.log("x nu este egal cu 5 si nici y nu este egal cu 3")
}




//LOOP-URI
let contor = 0

//!!!DE MENTIONAT -> DACA UITI SA INCREMENTEZI CONTORUL, AJUNGI LA O BUCLA INFINITA
//CONDITIE contor > 5
while(contor > 5){
    console.log(contor)
    contor++
}

contor = 0;
do
{
    console.log(contor);
    contor++
} while (contor < 5)
console.log(contor);

contor = 0;
for(contor; contor < 5; contor++)
{
    console.log(contor)
}

    //daca vrem sa mentionam si ca putem sa declaram contorul inline



//STRING-URI


//putem sa explicam exemple de ASA NU
// const badString1 = This is a test;
// const badString2 = 'This is a test;
// const badString3 = This is a test';


const string = "The revolution will not be televised.";
console.log(string);

const stringNou = string;
console.log(stringNou);


const sgl = 'Single quotes.'
const dbl = "Double quotes"
console.log(sgl)
console.log(dbl)


console.log("Numarul meu este " + x);

const greeting = `Hello,`
const yourName = "Miqi"
console.log(greeting + yourName);
//aici nu avem spatiu si explicam ce putem sa modificam ca sa fie ok
console.log(greeting + " " + yourName);


console.log("Numele meu are " + yourName.length + " litere")
//aratam metoda mai buna

console.log(`Numarul meu este ${x}`)

console.log(`Hello, ${yourName}`);


const betterGreeting = `Hello, ${yourName}! Ce mai faci?`
console.log(betterGreeting)



//ARRAYS


const arr = []


const nume = ['Ramon', 'Daria', 'Ramona', 'Dorin']

//pui intrebare cu ce cred recrutii ca va afisa, le explici de ce
console.log(nume[4])

//dupa arati ca array-ul este indexat de la 0 (numerotare incepe de la 0) si afisezi
console.log(nume[0])
console.log(nume[1])
console.log(nume[2])
console.log(nume[3])

console.log(nume.length);

for(let i = 0; i < nume.length; i++)
{
    console.log(nume[i]);
}

//porneste de la 0 si se termina la length-1


nume.push("Irina")
console.log(nume[4])

nume.pop()
console.log(nume[4])


//FUNCTII


console.log("Hello, " + "Dudu")
console.log("Hello, " + "Dudu1")
console.log("Hello, " + "Dudu2")
console.log("Hello, " + "Dudu3")

//ce face un parametru -> o variabila locala a functiei a carei valoare se seteaza la apelul functiei
function afisareNume(nume)
{
    console.log("Hello, " + nume)
}


afisareNume("Mihai")
afisareNume("DuduCuRosu")

for(let i = 0; i < nume.length; i++)
    afisareNume(nume[i])




//de prezentat ca orice e dupa return nu se ia in calcul
//aratam si ca putem schimba numele functiei si face tot acelasi lucru

function suma(a,b)
{
    let rezultat
    rezultat = a + b
    return rezultat
}

//transformam functia sa fie rezolvata doar din return
let rez = suma(10,30);
console.log(rez);
console.log(suma(10,30))

//aratam ca nu trebuie aceeasi denumire de variabile ca si parametru declarat in functie
let n = 15
let m = 25

console.log("Suma celor doua valori este: " + suma(n,m))

console.log("n este egal cu " + n)
console.log("m este egal cu " + m)


const sumaArrow = (a,b) => {
    return a + b
};

console.log(sumaArrow(n,m))


//OBIECTE

let obiect = {}

let masina = {
    marca: "CrocoMobil",
    culoare: "Verde",
    anFabricatie: 2010,
    asigurata: true,
    //Adaugam viteza si functiile
    viteza: 10,

    acceleratie: function()
    {
        viteza = viteza + 10;
    },

    frana: function()
    {
        viteza = 0;
    }
}

console.log(masina.marca)
console.log(masina.culoare)
console.log(masina.anFabricatie)
console.log(masina.asigurata)

//aratam ca putem sa modificam
masina.asigurata = false
console.log(masina.asigurata)

//aratam ca putem sa adaugam proprietati din cod
masina.numeProprietar = "Liviu"

//adaugi in obiect viteza si functiile
//spunem ca functiile din cadrul obiectelor se numesc metode
console.log(masina.viteza)

masina.acceleratie()
console.log(masina.viteza)

masina.frana()
console.log(masina.viteza)
